const prompts = require('prompts');

module.exports.menuCalculadora = ()=>(async ()=>{
    const response = await prompts(
        {
            type: 'select',
            name: 'value',
            message: 'Elegir por que método resolver:',
            choices: [
                { title: 'Suma', value: 'sumar' },
                { title: 'Resta', value: 'restar' },
                { title: 'Multiplicación', value: 'multiplicar' },
                { title: 'División', value: 'dividir' },
                { title: 'Operaciones Mixtas', value: 'operacionMultiple' },
            ],
        }
    );
    return response.value;
})();