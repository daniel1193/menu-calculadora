const prompts = require('prompts');

module.exports.pedirNumero = ()=>(async ()=>{
    const numero = await prompts(
        {
            type: 'number',
            name: 'value',
            message: 'Ingrese el numero'
        }
    );
    return numero.value;
})();