module.exports.funcionOpcion = (fun,numero1,numero2)=>fun(numero1,numero2);
module.exports.operacion ={
    sumar: (numero1,numero2)=>{
        const respuestaSuma = sumarCallback(numero1,numero2);
        return respuestaSuma;
    },
    restar: (numero1,numero2)=>{
        const respuestaSuma = restarCallback(numero1,numero2);
        return respuestaSuma;
    },
    multiplicar: (numero1,numero2)=>{
        const respuestaSuma = multiplicarCallback(numero1,numero2);
        return respuestaSuma;
    },
    dividir: (numero1,numero2)=>{
        const respuestaSuma = dividirCallback(numero1,numero2);
        return respuestaSuma;
    }
};

const sumarCallback = (numero1,numero2)=>{
  return numero1 + numero2;
};
const restarCallback = (numero1,numero2)=>{
    return numero1 - numero2;
};
const multiplicarCallback = (numero1,numero2)=>{
    return numero1 * numero2;
};
const dividirCallback = (numero1,numero2)=>{
    return numero1 / numero2;
};