const {menuCalculadora} = require('./menu-calculadora');
const {funcionOpcion,operacion} = require('./operacionesCallback');
const {pedirNumero} = require('./pedir-numeros');

module.exports.funcionesTipoCalculadora = (fun) => fun();

module.exports.tipoCalculadora = {
    callback: async ()=> {
        const opcion = await menuCalculadora();
        const numeroUno = await pedirNumero();
        const numeroDos = await pedirNumero();
        const resultado = await funcionOpcion(operacion[opcion],numeroUno,numeroDos);
        console.log(resultado);
    },
    promesas:  ()=>menuCalculadora(),
    observables:  ()=>menuCalculadora()
};