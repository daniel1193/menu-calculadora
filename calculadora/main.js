const EventEmitter = require('events');
const {menuPrincipal} = require('./menu-principal');

const mainCalculadora = new EventEmitter();

mainCalculadora.on('calculadora',()=>{
    menuPrincipal();
});

mainCalculadora.emit('calculadora');

