//Para usar el prompts para la interfaz por consola se debe instalar:
//npm install --save prompts
const prompts = require('prompts');

const {funcionesTipoCalculadora,tipoCalculadora} = require('./tipo-metodo');

module.exports.menuPrincipal = ()=>{
    (async ()=>{
        let confirmacion = true;
        do{
            const response = await prompts(
                {
                    type: 'select',
                    name: 'value',
                    message: 'Elegir por que método resolver:',
                    choices: [
                        { title: 'Callbacks', value: 'callback' },
                        { title: 'Promesas', value: 'promesas' },
                        { title: 'Observables', value: 'observables' }
                    ],
                }
            );
            const tipoRespuesta = response.value;
            await funcionesTipoCalculadora(tipoCalculadora[tipoRespuesta]);
            confirmacion = await prompts({
                type: 'confirm',
                name: 'value',
                message: 'Can you confirm?',
                initial: true
            });
        }while (confirmacion.value)
    })();
};

